/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  siteMetadata: {
    title: `mon-gatsby`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [],
}
